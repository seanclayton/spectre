import React from "react";
import styled, { css } from "react-emotion";
import SpNavbar from "./SpNavbar";

const Layout = styled("div")`
  display: grid;
`;

export class CoreContentLayout extends React.Component {
  render() {
    return (
      <section
        className={css`
          width: 100%;
          margin: 0 auto;

          @media screen and (min-width: 1440px) {
            max-width: 64em;
          }

          @media screen and (max-width: 1440px) {
            padding: 0 1em;
          }
        `}
      >
        {this.props.children}
      </section>
    );
  }
}

export default class CoreLayout extends React.Component {
  render() {
    return (
      <Layout>
        <SpNavbar />
        {this.props.children}
      </Layout>
    );
  }
}
