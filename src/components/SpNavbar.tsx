import React from "react";
import styled from "react-emotion";
import qs from "query-string";
import { NavLink as NLink, Link } from "react-router-dom";
import SpLogo from "./SpLogo";

const Nav = styled("nav")`
  display: grid;
  grid-template-areas:
    "header"
    "links";
  padding: 1em 0 2em;
`;

const NavHeader = styled("header")`
  grid-area: header;
  margin: 0 auto;
`;

const Logo = styled(SpLogo)`
  height: 24px;
`;

const LogoLink = styled(Link)`
  display: inline-grid;
  grid-auto-flow: column;
  justify-content: center;
  align-items: center;
  grid-gap: 0.5em;
  text-decoration: none;
  color: inherit;

  &:hover {
    text-decoration: none;
  }
`;

const NavHeading = styled("h1")`
  font-size: 24px;
  font-weight: 400;
  margin: 0;
`;

const NavLinks = styled("nav")`
  display: grid;
  justify-content: center;
  align-items: center;
  grid-area: links;
  grid-gap: 1em;
  grid-auto-flow: column;
`;

const NavLink = styled(NLink)`
  padding: 1em 0;
  color: #151d20;
  text-transform: uppercase;
  text-decoration: none;

  &:hover {
    text-decoration: none;
  }

  &.active {
    box-shadow: 0 2px 0 0 #07f;
  }
`;

export default class SpNavbar extends React.Component {
  render() {
    return (
      <Nav>
        <NavHeader>
          <LogoLink to="/">
            <NavHeading>SPECTRE</NavHeading>
            <Logo />
          </LogoLink>
        </NavHeader>
        <NavLinks>
          <NavLink exact to="/">
            Home
          </NavLink>
          <NavLink
            to={{
              pathname: "/weapons",
              search: qs.stringify({ skip: 0 }),
            }}
          >
            Weapons
          </NavLink>
          <NavLink
            to={{
              pathname: "/armor",
              search: qs.stringify({ skip: 0 }),
            }}
          >
            Armor
          </NavLink>
          <NavLink
            to={{
              pathname: "/items",
              search: qs.stringify({ skip: 0 }),
            }}
          >
            Items
          </NavLink>
        </NavLinks>
      </Nav>
    );
  }
}
