import React from "react";
import { Link } from "react-router-dom";
import styled from "react-emotion";
import _ from "lodash/fp";

const SpItemCard = styled("div")`
  > * {
    margin: 0;
    font-weight: normal;
  }
`;

const CardWrapper = styled(Link)<any>`
  text-decoration: none;
  padding: 1em;
  position: relative;
  background-color: ${({ theme, tier }) => {
    switch (tier) {
      case "Rare":
        return theme.blue.toString();
      case "Legendary":
        return theme.purple.toString();
      case "Exotic":
        return theme.yellow.toString();
      case "Common":
        return theme.green.toString();
      default:
        return theme.lightGray.toString();
    }
  }};
  color: ${({ theme, tier }) => {
    switch (tier) {
      case "Rare":
      case "Legendary":
      case "Common":
        return "white";
      default:
        return theme.gray0.toString();
    }
  }};
`;

export default class ItemCard extends React.Component {
  props: any;
  render() {
    return (
      <CardWrapper
        tier={_.get("tier.name")(this.props)}
        to={`/${this.props.itemType}/${this.props.hash}`}
      >
        <SpItemCard>
          <h4>{this.props.name}</h4>
        </SpItemCard>
      </CardWrapper>
    );
  }
}
