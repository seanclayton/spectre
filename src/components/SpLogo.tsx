import React from "react";

export default class SpLogo extends React.Component {
  render() {
    return (
      <svg
        viewBox="0 0 512 512"
        version="1"
        fillRule="evenodd"
        clipRule="evenodd"
        strokeLinejoin="round"
        strokeMiterlimit="1"
        {...this.props}
      >
        <path d="M248 0L40 208v96l208 208V352l-96-96 96-96V0zm15 0v160l96 96-96 96v160l209-208v-96L263 0zm-7 192a64 64 0 1 1 0 128 64 64 0 0 1 0-128z" />
      </svg>
    );
  }
}
