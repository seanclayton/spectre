import React from "react";
import _ from "lodash/fp";
import styled from "react-emotion";
import stringToReact from "src/utils/string-to-react";

const ItemWrapper = styled("section")`
  display: grid;
  grid-template-areas:
    "name name"
    "type type"
    "description description"
    "stats lore";
  grid-gap: 1em;
  grid-template-columns: 1fr 1fr;
`;

const ItemName = styled("h1")`
  grid-area: name;
`;

const ItemDescription = styled("p")`
  grid-area: description;
`;

const ItemType = styled("p")`
  grid-area: type;
`;

const ItemLore = styled("article")`
  grid-area: lore;
  opacity: 0.75;
  font-size: 0.9em;
`;

const ItemStats = styled("section")`
  grid-area: stats;
`;

class SpItem extends React.Component {
  render() {
    return (
      <ItemWrapper>
        <ItemName>{_.get("name")(this.props)}</ItemName>
        <ItemType>
          {_.get("tier.name")(this.props)} {_.get("itemTypeName")(this.props)}
        </ItemType>
        <ItemDescription>{_.get("description")(this.props)}</ItemDescription>
        <ItemStats>
          <h1>Stats</h1>
        </ItemStats>
        <ItemLore>
          {stringToReact(_.get("lore.description")(this.props))}
        </ItemLore>
      </ItemWrapper>
    );
  }
}

export default SpItem;
