import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import "typeface-chivo";
import "normalize.css/normalize.css";
import "./styles/app.css";

const renderApp = () => {
  ReactDOM.render(<App />, document.getElementById("root"));
};

renderApp();
registerServiceWorker();

if (module.hot) {
  module.hot.accept();
}
