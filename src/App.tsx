import React from "react";
import { Provider, Client } from "urql";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { ThemeProvider } from "emotion-theming";
import chroma from "chroma-js";
import HomeView from "src/views/HomeView";
import ItemsView from "src/views/ItemsView";
import ItemView from "src/views/ItemView";
import CoreLayout, { CoreContentLayout } from "src/components/CoreLayout";

const client = new Client({
  url: "https://db-api.destinytracker.com/api/graphql",
});

class App extends React.Component {
  render() {
    return (
      <Router>
        <ThemeProvider
          theme={{
            steelBlue: chroma("#29303c"),
            gray0: chroma("#151d20"),
            gray1: chroma("#232c2f"),
            lightGray: chroma("#e5e8ee"),
            gold: chroma("#fdcd47"),
            orange: chroma("#f77017"),
            purple: chroma("#532f66"),
            green: chroma("#056d36"),
            blue: chroma("#4880a1"),
            yellow: chroma("#d4ae00"),
          }}
        >
          <CoreLayout>
            <CoreContentLayout>
              <Provider client={client}>
                <Switch>
                  <Route exact path="/" component={HomeView} />
                  <Route exact path="/:itemType" component={ItemsView} />
                  <Route exact path="/:itemType/:itemId" component={ItemView} />
                </Switch>
              </Provider>
            </CoreContentLayout>
          </CoreLayout>
        </ThemeProvider>
      </Router>
    );
  }
}

export default App;
