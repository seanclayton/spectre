import React from "react";
import PageWrapper from "src/components/PageWrapper";

export default class HomeView extends React.Component {
  render() {
    return (
      <PageWrapper>
        <h1>Home</h1>
      </PageWrapper>
    );
  }
}
