import * as React from "react";
import { query, Connect } from "urql";
import SpItem from "src/components/SpItem";
import PageWrapper from "src/components/PageWrapper";

const gqlQuery = `
  query GetItem($hash: Hash) {
    item(hash: $hash) {
      hash
      name
      description
      itemTypeName
      lore {
        description
      }
      tier {
        name
      }
      stats {
        stats {
          name
          value
        }
      }
    }
  }
`;

export default class ItemView extends React.Component {
  props: {
    match: any;
  };
  render() {
    const GetItem = query(gqlQuery, { hash: this.props.match.params.itemId });
    return (
      <PageWrapper>
        <Connect query={GetItem}>
          {({ data, loaded, error }: any) => {
            if (loaded) {
              return <SpItem {...data.item} />;
            }
            if (error) {
              return (
                <pre>
                  <code>{JSON.stringify(error, null, 2)}</code>
                </pre>
              );
            }
            return null;
          }}
        </Connect>
      </PageWrapper>
    );
  }
}
