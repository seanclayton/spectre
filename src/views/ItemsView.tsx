import React from "react";
import _ from "lodash/fp";
import { query as gql, Connect } from "urql";
import styled from "react-emotion";
import queryString from "query-string";
import PageWrapper from "src/components/PageWrapper";
import ItemCard from "src/components/ItemCard";

const LinkList = styled("div")`
  display: grid;
  grid-template-columns: repeat(3, minmax(min-content, 1fr));
  grid-gap: 1em;
  grid-auto-flow: row dense;
  grid-auto-rows: 1fr;
`;

const query = `
  query Items($category: Hash, $skip: Int, $limit: Int) {
    itemsSearch(categories: [$category], limit: $limit, skip: $skip) {
      totalPageCount
      pageInfo {
        nextSkip
        previousSkip
        hasNextPage
        hasPreviousPage
        currentPage
      }
      nodes {
        hash
        name
        tier {
          hash
          name
        }
      }
    }
  }
`;

type P = any;
type S = {
  skip: number;
};

const PAGE_SIZE = 15;

export default class ItemsView extends React.Component<P, S> {
  props: any;
  state = {
    skip: 0,
  };
  setSkip = (skip: number) => {
    this.setState({
      skip,
    });
  };
  static getDerivedStateFromProps(props: any) {
    const { location } = props;
    const qs = queryString.parse(location.search);
    if (Boolean(_.get("skip")(qs))) {
      return {
        skip: _.get("skip")(qs),
      };
    }
    return {
      skip: 0,
    };
  }
  componentDidMount() {
    const { location, history } = this.props;
    const qs = queryString.parse(location.search);
    if (!Boolean(_.get("skip")(qs))) {
      history.push(`/${_.get("match.params.itemType")(this.props)}?skip=0`);
    }
  }
  render() {
    const categoryMap = {
      items: 52,
      weapons: 1,
      armor: 20,
    };
    const category = categoryMap[_.get("match.params.itemType")(this.props)];
    return (
      <PageWrapper>
        <Connect
          query={gql(query, {
            category,
            skip: this.state.skip,
            limit: PAGE_SIZE,
          })}
        >
          {({ data, loaded, error, fetching }: any) => {
            if (loaded && !fetching) {
              return (
                <>
                  <LinkList>
                    {_.flow(
                      _.get("itemsSearch.nodes"),
                      _.map((item: any) => (
                        <ItemCard
                          key={item.hash}
                          history={this.props.history}
                          itemType={this.props.match.params.itemType}
                          {...item}
                        />
                      )),
                    )(data)}
                  </LinkList>
                  <div>
                    <button
                      disabled={
                        !_.get("itemsSearch.pageInfo.hasPreviousPage")(data)
                      }
                      onClick={() =>
                        this.props.history.push(
                          `/${_.get("match.params.itemType")(
                            this.props,
                          )}?skip=${_.get("itemsSearch.pageInfo.previousSkip")(
                            data,
                          )}`,
                        )
                      }
                    >
                      Prev
                    </button>
                    <span>
                      {_.get("itemsSearch.pageInfo.currentPage")(data)} /{" "}
                      {_.get("itemsSearch.totalPageCount")(data)}
                    </span>
                    <button
                      disabled={
                        !_.get("itemsSearch.pageInfo.hasNextPage")(data)
                      }
                      onClick={() =>
                        this.props.history.push(
                          `/${_.get("match.params.itemType")(
                            this.props,
                          )}?skip=${_.get("itemsSearch.pageInfo.nextSkip")(
                            data,
                          )}`,
                        )
                      }
                    >
                      Next
                    </button>
                  </div>
                </>
              );
            }
            if (error) {
              return (
                <pre>
                  <code>{JSON.stringify(error, null, 2)}</code>
                </pre>
              );
            }
            return null;
          }}
        </Connect>
      </PageWrapper>
    );
  }
}
