import React from "react";
import _ from "lodash/fp";

export default _.flow(
  _.split("\n"),
  _.entries,
  _.map(([key, item]) => <p key={key}>{item}</p>),
);
